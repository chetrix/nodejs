const express = require("express");
const bodyParser = require("body-parser");
const device = require("express-device");
const path = require("path");
// const expressHbs = require('express-handlebars');
const app = express();
const mongoose = require('mongoose');

// app.set('view engine', 'pug');
// app.engine('hbs', expressHbs({
//     layoutsDir: 'views/layouts/',
//     defaultLayout: 'main-layout',
//     extname: 'hbs'
// }));
// app.set('view engine', 'hbs');
app.set("view engine", "ejs");
app.set("views", "views"); // Telling express where templates can be found

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");
const errorController = require("./controllers/error");
// const sequelize = require("./util/database");
// const mongoConnect = require('./util/database').mongoConnect;
const User = require('./models/user');

// const Product = require("./models/product");
// const User = require("./models/user");
// const Cart = require("./models/cart");
// const CartItem = require("./models/cart-item");
// const Order = require("./models/order");
// const OrderItem = require("./models/order-item");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

//Middleware for send user info for every request. 
app.use((req,res,next)=> {
    User.findById('62c30031638e3e9a7d4fb16a').then(user=>{
        //this req.user is not just javascript object. It also has sequelize functionality(helper method) like detroy. with which you can delete user from DB.
        // req.user = new User(user.name, user.email, user.cart, user._id);
        //Mongoose way
        req.user = user;
        next();
    }).catch(err=>console.log(err))
})

app.use(device.capture({ parseUserAgent: true }));
app.use("/admin", adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

// //CREATING RELATIONS

// Product.belongsTo(User, { constraints: true, onDelete: "CASCADE" });
// User.hasMany(Product); //Optional. could be used in above line instead of belongsTo

// Cart.belongsTo(User);
// User.hasOne(Cart);  //Inverse of the upper relation which obviously optional. one direction is enough.

// Cart.belongsToMany(Product, {through: CartItem});  //many to many relation. cart can contain many products. This only works with an intermediate table that connects them which basically stores a combination of product ids and cart ids.
// Product.belongsToMany(Cart, {through: CartItem});  // many to many relation. Product can be part be many carts. This only works with an intermediate table that connects them which basically stores a combination of product ids and cart ids.

// Order.belongsTo(User);
// User.hasMany(Order);

// Order.belongsToMany(Product, {through: OrderItem});

// sequelize
//   .sync() //.sync({force: true}) // To overwrite the table which shouldnt be happening in production
//   .then((result) => {
//     //Syncs our models to the database by creating the appropriate tables and if we have them relations
//     return User.findByPk(1);
//   })
//   .then((user) => {
//     if (!user) {
//       return User.create({ name: "Amit", email: "test@test.com" });
//     }
//     return user;
//   })
//   .then(user=>{
//     user.getCart().then((cart)=>{
//       if(cart){
//         return user;
//       }else{
//         return user.createCart();
//       }
//     }).catch(err=>console.log(err))
//     // return user.createCart();
//   })
//   .then((user) => {
//     // console.log(user);
//     app.listen(3000);
//   })
//   .catch((err) => console.log(err));

// mongoConnect(()=> {

//   app.listen(3000);
// })
mongoose.connect('mongodb+srv://chetrix:rtz5kw55555@devconnector.wphwk.mongodb.net/test').then(result=>{
  User.findOne().then(user=> {
    if(!user){
      const user = new User({
        name: 'Max',
        email: 'max@test.com',
        cart: {
          items: []
        }
      })
      user.save();
    }
  })
  app.listen(3000);
}).catch(err=>{
  console.log(err);
})
