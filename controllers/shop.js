// const products = [];
const Product = require("../models/product");
const Order = require('../models/order');
// const Cart = require("../models/cart");
// const Order = require("../models/order");

// exports.getProducts = (req, res, next) => {
//     // const products = adminData.products;
//     // const products = Product.fetchAll();
//     Product.fetchAll((products)=>{
//         res.render('shop/product-list', {
//             prods: products,
//             pageTitle: 'All products',
//             path:'/products',
//             // hasProducts: products.length>0, //hbs
//             // activeShop: true, // hbs
//             // productCSS: true, //hbs
//             // layout: true // to disable main-layout set it to false
//         });
//     })
// }

//DATABASE>>>>>>>>>>>>

// exports.getProducts = (req, res, next) => {
//     // const products = adminData.products;
//     // const products = Product.fetchAll();
//     Product.fetchAll().then(([rows, fieldData])=>{
//         res.render('shop/product-list', {
//             prods: rows,
//             pageTitle: 'All products',
//             path:'/products',
//         });
//     }).catch(err=>console.log(err))
// }

//SEQUELIZE>>>>>>>>>>>>>>>>>>>

// exports.getProducts = (req, res, next) => {
//   Product.findAll()
//     .then((products) => {
//       res.render("shop/product-list", {
//         prods: products,
//         pageTitle: "All products",
//         path: "/products",
//       });
//     })
//     .catch((err) => console.log(err));
// };

// MONGODB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// exports.getProducts = (req, res, next) => {
//   Product.fetchAll()
//     .then((products) => {
//       res.render("shop/product-list", {
//         prods: products,
//         pageTitle: "All products",
//         path: "/products",
//       });
//     })
//     .catch((err) => console.log(err));
// }

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

exports.getProducts = (req, res, next) => {
  Product.find()
    .then((products) => {
      res.render("shop/product-list", {
        prods: products,
        pageTitle: "All products",
        path: "/products",
      });
    })
    .catch((err) => console.log(err));
}

// exports.getProduct = (req, res, next) =>{
//     const prodId = req.params.productId;
//     Product.findById(prodId, product => {
//         res.render('shop/product-detail', {
//             product: product,
//             pageTitle: product.title,
//             path:'/products',
//             // hasProducts: products.length>0,
//             // activeShop: true,
//             // productCSS: true,
//             // layout: true // to disable main-layout set it to false
//         });
//     })
// }

//DATABASE>>>>>>>>>>>>>>>>>>

// exports.getProduct = (req, res, next) =>{
//     const prodId = req.params.productId;
//     Product.findById(prodId).then(([product])=>{
//         res.render('shop/product-detail', {
//             product: product[0],
//             pageTitle: product.title,
//             path:'/products',
//         });
//     }).catch(err=>console.log(err))
// }

//SEQUELIZE>>>>>>>>>>>>>

// exports.getProduct = (req, res, next) => {
//   const prodId = req.params.productId;
//   // Product.findByPk(prodId).then(product=>{               //changed from findById to findByPk since sequelize v5+
//   //     res.render('shop/product-detail', {
//   //         product: product,
//   //         pageTitle: product.title,
//   //         path:'/products',
//   //     });
//   // }).catch(err=>console.log(err));

//   //ANOTHER APPROACH WITH WHERE KEYWORD>>>>>>>>>>>>>>>>>>

//   Product.findAll({ where: { id: prodId } })
//     .then((product) => {
//       //changed from findById to findByPk since sequelize v5+
//       res.render("shop/product-detail", {
//         product: product[0],
//         pageTitle: product[0].title,
//         path: "/products",
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MONGODB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// exports.getProduct = (req, res, next) => {
//   const prodId = req.params.productId;
//   Product.findById(prodId)
//     .then((product) => {
//       res.render("shop/product-detail", {
//         product: product,
//         pageTitle: product.title,
//         path: "/products",
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
exports.getProduct = (req, res, next) => {
  const prodId = req.params.productId;
  Product.findById(prodId)   //findById is defined by mongoose
    .then((product) => {
      res.render("shop/product-detail", {
        product: product,
        pageTitle: product.title,
        path: "/products",
      });
    })
    .catch((err) => console.log(err));
};

// exports.getIndex = (req, res, next) => {
//     Product.fetchAll((products)=>{
//         res.render('shop/index', {
//             prods: products,
//             pageTitle: 'Shop',
//             path:'/',
//             // hasProducts: products.length>0,
//             // activeShop: true,
//             // productCSS: true,
//             // layout: true // to disable main-layout set it to false
//         });
//     })
// }

//DATABASE>>>>>>>>>>>>>>>>>>>
// exports.getIndex = (req, res, next) => {
//     Product.fetchAll().then(([rows, fieldData])=>{
//         res.render('shop/index', {
//             prods: rows,
//             pageTitle: 'Shop',
//             path:'/',
//         });
//     }).catch(err=>console.log(err))

// }

//SEQUELIZE>>>>>>>>>>>>>>>>

// exports.getIndex = (req, res, next) => {
//   Product.findAll()
//     .then((products) => {
//       res.render("shop/index", {
//         prods: products,
//         pageTitle: "Shop",
//         path: "/",
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MONGODB >>>>>>>>>>>>>>>>>>>>>>>>>

// exports.getIndex = (req, res, next) => {
//   Product.fetchAll()
//     .then((products) => {
//       res.render("shop/index", {
//         prods: products,
//         pageTitle: "Shop",
//         path: "/",
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>

exports.getIndex = (req, res, next) => {
  Product.find()
    .then((products) => {
      res.render("shop/index", {
        prods: products,
        pageTitle: "Shop",
        path: "/",
      });
    })
    .catch((err) => console.log(err));
};


// FILESYSTEM  >>>>>>>>>>>>>>>>>>>>
// exports.getCart = (req, res, next) => {
//     Cart.getCart(cart=>{
//         Product.fetchAll(products=>{
//             const cartProducts = [];
//             for(product of products){
//                 const cartProductData = cart.products.find(prod => prod.id === product.id);
//                 if(cartProductData){
//                     cartProducts.push({productData: product, qty: cartProductData.qty});
//                 }
//             }
//             res.render('shop/cart',{
//                 path: '/cart',
//                 pageTitle: 'Your Cart',
//                 products: cartProducts
//             });
//         })
//     })
// }
// SEQUELIZE >>>>>>>>>>>>>>

// exports.getCart = (req, res, next) => {
//   req.user
//     .getCart()
//     .then((cart) => {
//       return cart
//         .getProducts()
//         .then((products) => {
//           res.render("shop/cart", {
//             path: "/cart",
//             pageTitle: "Your Cart",
//             products: products,
//           });
//         })
//         .catch((err) => console.log(err));
//     })
//     .catch((err) => console.log(err));
// };

//MONGODB >>>>>>>>>>>>>>>>>>>>>>>

// exports.getCart = (req, res, next) => {
//   req.user
//     .getCart()
//     .then(products => {
//           res.render("shop/cart", {
//             path: "/cart",
//             pageTitle: "Your Cart",
//             products: products,
//           });
//         })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>>

exports.getCart = (req, res, next) => {
  req.user
    .populate('cart.items.productId')
    // .execPopulate()    // since .populate doesnot give promise. so we have to implement execPopulate.
    .then(user => {
      const products = user.cart.items;
          res.render("shop/cart", {
            path: "/cart",
            pageTitle: "Your Cart",
            products: products,
          });
        })
    .catch((err) => console.log(err));
};

//FILESYSTEM >>>>>>>>>>>>>>>>
// exports.postCart = (req, res, next) => {
//   const prodId = req.body.productId;

//   Product.findById(prodId, (product) => {
//     Cart.addProduct(prodId, product.price);
//   });
//   res.redirect("/cart");
// };

//SEQUELIZE >>>>>>>>>>>>>>>
// exports.postCart = (req, res, next) => {
//   const prodId = req.body.productId;
//   let fetchedCart;
//   req.user
//     .getCart()
//     .then((cart) => {
//       fetchedCart = cart;
//       return cart.getProducts({ where: { id: prodId } });
//     })
//     .then((products) => {
//       let product;
//       if (products.length > 0) {
//         product = products[0];
//       }
//       let newQuantity = 1;
//       if (product) {
//         const oldQuantity = product.cartItem.quantity;
//         newQuantity = oldQuantity + 1;
//         return fetchedCart.addProduct(product, {
//           through: { quantity: newQuantity },
//         });
//       }
//       return Product.findByPk(prodId)
//         .then((product) => {
//           return fetchedCart.addProduct(product, {
//             through: { quantity: newQuantity },
//           });
//         })
//         .catch((err) => console.log(err));
//     })
//     .then(() => {
//       res.redirect("/cart");
//     })
//     .catch((err) => console.log(err));
// };

// MONGODB >>>>>>>>>>>>>>>>>>>>>
// exports.postCart = (req, res, next) => {
//   const prodId = req.body.productId;
//   Product.findById(prodId).then(product=> {
//     return req.user.addToCart(product);
//   })
//     .then(result=> {
//       res.redirect("/cart");
//     })    
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>

exports.postCart = (req, res, next) => {
  const prodId = req.body.productId;
  Product.findById(prodId).then(product=> {
    return req.user.addToCart(product);
  })
    .then(result=> {
      res.redirect("/cart");
    })    
};

//FileSystem >>>>>>>>>>>>>>>>>>>>

// exports.postCartDeleteProduct = (req, res, next) => {
//   const prodId = req.body.productId;
//   Product.findById(prodId, (product) => {
//     Cart.deleteProduct(prodId, product.price);
//     res.redirect("/cart");
//   });
// };

//SEQUELIZE >>>>>>>>>>>>>>>>>>>>>>
// exports.postCartDeleteProduct = (req, res, next) => {
//   const prodId = req.body.productId;
//   req.user
//     .getCart()
//     .then((cart) => {
//       return cart.getProducts({ where: { id: prodId } });
//     })
//     .then((products) => {
//       const product = products[0];
//       return product.cartItem.destroy();
//     })
//     .then(() => {
//       res.redirect("/cart");
//     })
//     .catch((err) => console.log(err));
// };

//MONGODB >>>>>>>>>>>>>>>>>>>>>>
// exports.postCartDeleteProduct = (req, res, next) => {
//   const prodId = req.body.productId;
//   req.user
//     .deleteItemFromCart(prodId)
//     .then(() => {
//       res.redirect("/cart");
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>
exports.postCartDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  req.user
    .deleteItemFromCart(prodId)
    .then(() => {
      res.redirect("/cart");
    })
    .catch((err) => console.log(err));
};

// exports.postOrder = (req, res, next) => {
//   let fetchedCart;
//   req.user
//     .getCart()
//     .then((cart) => {
//       fetchedCart = cart;
//       return cart.getProducts();
//     })
//     .then((products) => {
//       return req.user
//         .createOrder()
//         .then((order) => {
//           order.addProduct(
//             products.map((product) => {
//               product.orderItem = { quantity: product.cartItem.quantity };
//               return product;
//             })
//           );
//         })
//         .catch((err) => console.log(err));
//     })
//     .then((result) => {
//       fetchedCart.setProducts(null);
//       res.redirect("/orders");
//     })
//     .catch((err) => console.log(err));
// };

//MONGODB >>>>>>>>>>>>>>>>>>>>>>>>

// exports.postOrder = (req, res, next) => {
//   let fetchedCart;
//   req.user
//     .addOrder()
//     .then((result) => {
//       res.redirect("/orders");
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>

exports.postOrder = (req, res, next) => {
  req.user
    .populate('cart.items.productId')
    // .execPopulate()    // since .populate doesnot give promise. so we have to implement execPopulate.
    .then(user => {
      const products = user.cart.items.map(i=>{
        return {quantity: i.quantity, product: {...i.productId._doc}}  //._doc to give product detail with the id
      })
      const order = new Order({
        user: {
          name: req.user.name,
          userId: req.user           //Mongoose will pick the id
        },
        products: products
    });
    return order.save();
    })
    .then((result) => {
      return req.user.clearCart();
    })
    .then(()=>{
      res.redirect("/orders");
    })
    .catch((err) => console.log(err));
};

//FILESYSTEM >>>>>>>>>>>>>>>>>>>>>>

// exports.getOrders = (req, res, next) => {
//   res.render("shop/orders", {
//     path: "/orders",
//     pageTitle: "Your orders",
//   });
// };

//SEQUELIZE

// exports.getOrders = (req, res, next) => {
//   req.user
//     .getOrders({include: ['products']})
//     .then((orders) => {
//       res.render("shop/orders", {
//         path: "/orders",
//         pageTitle: "Your orders",
//         orders: orders
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MONGODB >>>>>>>>>>>>>>>>>>>>>
// exports.getOrders = (req, res, next) => {
//   req.user
//     .getOrders()
//     .then((orders) => {
//       res.render("shop/orders", {
//         path: "/orders",
//         pageTitle: "Your orders",
//         orders: orders
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE 
exports.getOrders = (req, res, next) => {
  Order.find({"user.userId": req.user._id})
    .then((orders) => {
      res.render("shop/orders", {
        path: "/orders",
        pageTitle: "Your orders",
        orders: orders
      });
    })
    .catch((err) => console.log(err));
};

// exports.getCheckout = (req, res, next) => {
//   res.render("shop/checkout", {
//     path: "/checkout",
//     pageTitle: "Checkout",
//   });
// };

// exports.getCheckout = (req, res, next) => {
//   res.render("shop/checkout", {
//     path: "/checkout",
//     pageTitle: "Checkout",
//   });
// };
