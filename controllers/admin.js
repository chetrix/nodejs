const path = require("path");
const Product = require("../models/product");

exports.getAddProduct = (req, res, next) => {
  // res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
  res.render("admin/edit-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    editing: false,
  });
};

//FILE SYSTEM >>>>>>>>>>>>>>>>>>>>

// exports.getEditProduct = (req, res, next) => {
//   const editMode = req.query.edit;

//   if (!editMode) {
//     return res.redirect("/");
//   }
//   const prodId = req.params.productId;
//   Product.findById(prodId, (product) => {
//     if (!product) {
//       return res.redirect("/");
//     }

//     res.render("admin/edit-product", {
//       pageTitle: "Edit Product",
//       path: "/admin/edit-product",
//       editing: editMode,
//       product: product,
//     });
//   });
//   // res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
// };

//SEQUELIZE>>>>>>>>>>>>>>>>>>>>

// exports.getEditProduct = (req, res, next) => {
//   const editMode = req.query.edit;

//   if (!editMode) {
//     return res.redirect("/");
//   }
//   const prodId = req.params.productId;

//   req.user.getProducts({ where: { id: prodId } })
//   // Product.findByPk(prodId)
//     .then((products) => {
//       const product = products[0];     //Sequelize get us an array even if we hold only one element
//       if (!product) {
//         return res.redirect("/");
//       }

//       res.render("admin/edit-product", {
//         pageTitle: "Edit Product",
//         path: "/admin/edit-product",
//         editing: editMode,
//         product: product,
//       });
//     })
//     .catch((err) => console.log(err));
//   // res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
// };

//MONGODB >>>>>>>>>>>>>>>>>>>>>>>

// exports.getEditProduct = (req, res, next) => {
//   const editMode = req.query.edit;

//   if (!editMode) {
//     return res.redirect("/");
//   }
//   const prodId = req.params.productId;

//   Product.findById(prodId)
//   // Product.findByPk(prodId)
//     .then((product) => {
//       if (!product) {
//         return res.redirect("/");
//       }

//       res.render("admin/edit-product", {
//         pageTitle: "Edit Product",
//         path: "/admin/edit-product",
//         editing: editMode,
//         product: product,
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>>

exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;

  if (!editMode) {
    return res.redirect("/");
  }
  const prodId = req.params.productId;

  Product.findById(prodId)
  // Product.findByPk(prodId)
    .then((product) => {
      if (!product) {
        return res.redirect("/");
      }

      res.render("admin/edit-product", {
        pageTitle: "Edit Product",
        path: "/admin/edit-product",
        editing: editMode,
        product: product,
      });
    })
    .catch((err) => console.log(err));
};

//FILE SYSTEM>>>>>>>>>>>>>>>>>>>>>>>

// exports.postEditProduct = (req, res, next) => {
//   const prodId = req.body.productId;

//   const updatedTitle = req.body.title;
//   const updatedPrice = req.body.price;
//   const updatedImageUrl = req.body.imageUrl;
//   const updatedDesc = req.body.description;
//   const updatedProduct = new Product(
//     prodId,
//     updatedTitle,
//     updatedImageUrl,
//     updatedDesc,
//     updatedPrice
//   );

//   updatedProduct.save();
//   res.redirect("/admin/products");
// };

//SEQUELIZE>>>>>>>>>>>>>>>>>>>>>>>>>>

// exports.postEditProduct = (req, res, next) => {
//   const prodId = req.body.productId;
//   const updatedTitle = req.body.title;
//   const updatedPrice = req.body.price;
//   const updatedImageUrl = req.body.imageUrl;
//   const updatedDesc = req.body.description;

//   Product.findByPk(prodId)
//     .then((product) => {
//       product.title = updatedTitle;
//       product.price = updatedPrice;
//       product.description = updatedDesc;
//       product.imageUrl = updatedImageUrl;

//       return product.save(); //Save function provided by sequelize and saves in the database.
//     })
//     .then((result) => {
//       console.log("UPDATED PRODUCT!");
//       res.redirect("/admin/products");
//     })
//     .catch((err) => console.log(err));
// };

//MONGODB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// exports.postEditProduct = (req, res, next) => {
//   const prodId = req.body.productId;
//   const updatedTitle = req.body.title;
//   const updatedPrice = req.body.price;
//   const updatedImageUrl = req.body.imageUrl;
//   const updatedDesc = req.body.description;

//   const product = new Product(updatedTitle, updatedPrice, updatedDesc, updatedImageUrl, prodId)
//    product.save()
//     .then((result) => {
//       console.log("UPDATED PRODUCT!");
//       res.redirect("/admin/products");
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
exports.postEditProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDesc = req.body.description;

  Product.findById(prodId).then(product=> {
    product.title = updatedTitle;
    product.price = updatedPrice;
    product.description = updatedDesc;
    product.imageUrl = updatedImageUrl;
    return product.save()
  })
    .then((result) => {
      console.log("UPDATED PRODUCT!");
      res.redirect("/admin/products");
    })
    .catch((err) => console.log(err));
};

// exports.postAddProduct = (req, res, next) => {
//   // products.push({title: req.body.title})
//   const title = req.body.title;
//   const imageUrl = req.body.imageUrl;
//   const price = req.body.price;
//   const description = req.body.description;

//   const product = new Product(null, title, imageUrl, description, price);
//   product.save();
//   res.redirect("/");
// };

//DATABASE>>>>>>>>>>>>>>>>>

// exports.postAddProduct = (req, res, next) => {
//   // products.push({title: req.body.title})
//   const title = req.body.title;
//   const imageUrl = req.body.imageUrl;
//   const price = req.body.price;
//   const description = req.body.description;

//   const product = new Product(null, title, imageUrl, description, price);
//   product
//     .save()
//     .then(() => {
//       res.redirect("/");
//     })
//     .catch((err) => console.log(err));
// };

//SEQUELIZE>>>>>>>>>>

// exports.postAddProduct = (req, res, next) => {
//   // products.push({title: req.body.title})
//   const title = req.body.title;
//   const imageUrl = req.body.imageUrl;
//   const price = req.body.price;
//   const description = req.body.description;

//   req.user //req.user.createproduct  because of the has many and belongto relation, sequelize enable the model object to create another model object.
//     .createProduct({
//       title: title,
//       price: price,
//       imageUrl: imageUrl,
//       description: description,
//     })
//     .then((result) => {
//       console.log("created a product!");
//       res.redirect("/admin/products");
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// };

// MongoDB >>>>>>>>>>>>>>>>>>>>>>>>>

// exports.postAddProduct = (req, res, next) => {
//   const title = req.body.title;
//   const imageUrl = req.body.imageUrl;
//   const price = req.body.price;
//   const description = req.body.description;
//   const product = new Product(title, price, description, imageUrl, null, req.user._id);
//     product.save().then((result) => {
//       console.log("created a product!");
//       res.redirect("/admin/products");
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  const product = new Product({title: title, price: price, description: description,imageUrl: imageUrl,userId: req.user});  // we can also send req.user instead of req.user._id since mongoose can identify based on "type: Schema.Types.ObjectId" defined in models
    product.
    save()  //save method is not defined by us. It is provided by mongoose
    .then((result) => {
      console.log("created a product!");
      res.redirect("/admin/products");
    })
    .catch((err) => {
      console.log(err);
    });
};

//FILE BASED>>>>>>>>>>>>>>>>>

// exports.getProducts = (req, res, next) => {
//   Product.fetchAll((products) => {
//     res.render("admin/products", {
//       prods: products,
//       pageTitle: "Admin products",
//       path: "/admin/products",
//     });
//   });
// };

//SEQUELIZE>>>>>>>>>>>>>>

// exports.getProducts = (req, res, next) => {
//   req.user.getProducts()
//   // Product.findAll()
//     .then((products) => {
//       res.render("admin/products", {
//         prods: products,
//         pageTitle: "Admin products",
//         path: "/admin/products",
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MongoDB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// exports.getProducts = (req, res, next) => {
//   Product.fetchAll()
//   // Product.findAll()
//     .then((products) => {
//       res.render("admin/products", {
//         prods: products,
//         pageTitle: "Admin products",
//         path: "/admin/products",
//       });
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>
exports.getProducts = (req, res, next) => {
  Product.find()
  // Product.findAll()
    .then((products) => {
      res.render("admin/products", {
        prods: products,
        pageTitle: "Admin products",
        path: "/admin/products",
      });
    })
    .catch((err) => console.log(err));
};

//FILE SYSTEM >>>>>>>>>>>>>>>>>>>>>>

// exports.postDeleteProduct = (req, res, next) => {
//   const prodId = req.body.productId;
//   Product.deleteById(prodId);
//   res.redirect("/admin/products");
// };

//SEQUELIZE>>>>>>>>>>>>>>>>>>>>>

// exports.postDeleteProduct = (req, res, next) => {
//   const prodId = req.body.productId;
//   Product.findByPk(prodId)
//     .then((product) => {
//       return product.destroy();
//     })
//     .then((result) => {
//       console.log("DESTROYED PRODUCT");
//       res.redirect("/admin/products");
//     })
//     .catch((err) => console.log(err));
// };

//MongoDB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// exports.postDeleteProduct = (req, res, next) => {
//   const prodId = req.body.productId;
//    Product.deleteById(prodId)
//     .then(() => {
//       console.log("DESTROYED PRODUCT");
//       res.redirect("/admin/products");
//     })
//     .catch((err) => console.log(err));
// };

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>
exports.postDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
   Product.findByIdAndRemove(prodId)
    .then(() => {
      console.log("DESTROYED PRODUCT");
      res.redirect("/admin/products");
    })
    .catch((err) => console.log(err));
};
