// // const products = [];

// const fs = require("fs");
// const path = require("path");
// const p = path.join(
//   path.dirname(process.mainModule.filename),
//   "data",
//   "products.json"
// );
// const Cart = require("./cart");
// const getProductsFromFile = (cb) => {
//   fs.readFile(p, (err, fileContent) => {
//     //express file and node js read and write file
//     if (err) {
//       cb([]);
//     } else {
//       cb(JSON.parse(fileContent));
//     }
//   });
// };

// module.exports = class Product {
//   constructor(id, title, imageUrl, description, price) {
//     this.id = id;
//     this.title = title;
//     this.imageUrl = imageUrl;
//     this.description = description;
//     this.price = price;
//   }
//   save() {
//     // products.push(this);

//     // getProductsFromFile(products=>{      This can also be done instead of line no 35 to 44
//     //     products.push(this);
//     //     fs.writeFile(p, JSON.stringify(products), err=>{
//     //         console.log(err);
//     //     });
//     // });
//     const p = path.join(
//       path.dirname(process.mainModule.filename),
//       "data",
//       "products.json"
//     );
//     fs.readFile(p, (err, fileContent) => {
//       let products = [];
//       if (!err) {
//         products = JSON.parse(fileContent);
//       }

//       if (this.id) {
//         const existingProductIndex = products.findIndex(
//           (prod) => prod.id === this.id
//         );
//         const updatedProducts = [...products];
//         updatedProducts[existingProductIndex] = this;
//         fs.writeFile(p, JSON.stringify(updatedProducts), (err) => {
//           console.log(err);
//         });
//       } else {
//         this.id = Math.random().toString();

//         products.push(this);
//         fs.writeFile(p, JSON.stringify(products), (err) => {
//           console.log(err);
//         });
//       }
//     });
//   }
//   static fetchAll(cb) {
//     // static to call the function without creating any object
//     getProductsFromFile(cb);
//   }

//   static findById(id, cb) {
//     getProductsFromFile((products) => {
//       const product = products.find((p) => p.id === id);
//       cb(product);
//     });
//   }

//   static deleteById(id) {
//     getProductsFromFile((products) => {
//       const product = products.find((prod) => prod.id === id);
//       const updatedProducts = products.filter((prod) => prod.id !== id);
//       fs.writeFile(p,JSON.stringify(updatedProducts), (err) => {
//         if (!err) {
//           Cart.deleteProduct(id, product.price);
//         }
//       });
//     });
//   }
// };

//DATABASES>>>>>>>>>>

// const db = require('../util/database');

// const Cart = require("./cart");

// module.exports = class Product {
//   constructor(id, title, imageUrl, description, price) {
//     this.id = id;
//     this.title = title;
//     this.imageUrl = imageUrl;
//     this.description = description;
//     this.price = price;
//   }
//   save() {
//    return db.execute('INSERT INTO products (title, price,imageUrl, description) VALUES (?,?,?,?)',
//    [this.title, this.price, this.imageUrl, this.description]
//    );
//   }

//   static fetchAll() {
//     return db.execute('SELECT * FROM products');
//   }

//   static findById(id) {
//     return db.execute('SELECT * FROM products WHERE products.id = ?', [id]);
//   }

//   static deleteById(id) {
//   }
// };

//SEQUELIZE >>>>>>>>>>>>>>

// const Sequelize = require('sequelize');

// const sequelize = require('../util/database');

// const Product = sequelize.define('product', {
//   id:{
//     type: Sequelize.INTEGER,
//     autoIncrement:true,
//     allowNull: false,
//     primaryKey: true
//   },
//   title: Sequelize.STRING,
//   price: {
//     type: Sequelize.DOUBLE,
//     allowNull: false
//   },
//   imageUrl: {
//     type: Sequelize.STRING,
//     allowNull: false
//   },
//   description: {
//     type: Sequelize.STRING,
//     allowNull: false
//   }
// })

// module.exports = Product;

// MONGODB >>>>>>>>>>>>>>>>>>>>>>>>>>

// const mongodb = require('mongodb');
// const getDb = require("../util/database").getDb;

// class Product {
//   constructor(title, price, description, imageUrl, id, userId) {
//     this.title = title;
//     this.imageUrl = imageUrl;
//     this.description = description;
//     this.price = price;
//     this._id = id? new mongodb.ObjectId(id) : null;
//     this.userId = userId;
//   }
//   save() {
//     const db = getDb();
//     let dbOp;
//     if(this._id){
//          dbOp = db.collection('products').updateOne({_id: new mongodb.ObjectId(this._id)}, {$set: this})  // or {$set: {title: this.title,...}}
//     }else{
//       dbOp = db.collection('products')
//          .insertOne(this)
//     }
//     return dbOp
//       .then(result=> {
//       })
//       .catch((err) => console.log(err));
//   }

//   static fetchAll() {
//     const db = getDb();
//     return db
//       .collection('products')
//       .find()
//       .toArray()
//       .then(products=>{
//         return products;
//       })
//       .catch(err=> {
//         console.log(err);
//       })  
//   }

//   static findById(prodId) {
//     const db = getDb();
//     return db.collection('products')
//       .find({_id: new mongodb.ObjectId(prodId)})
//       .next()
//       .then(product => {
//         return product;
//       })
//   }

//   static deleteById(prodId) {
//     const db =getDb();
//     return db.collection('products').deleteOne({_id: new mongodb.ObjectId(prodId)})
//       .then(result=>{
//         console.log('Deleted!!!');
//       })
//       .catch(err=>console.log(err))
//   }
// }

// module.exports = Product;

//MONGOOSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',  //ref is useful when populating data for which only ids are stored as a relations
    required: true
  }
});

module.exports = mongoose.model('Product', productSchema)


