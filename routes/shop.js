const express = require('express');
// const path = require('path')
// const rootDir = require('../util/path');

// const adminData = require('./admin');
const shopController = require('../controllers/shop');
const { route } = require('./admin');

const router = express.Router()

router.get('/', shopController.getIndex);

router.get('/products', shopController.getProducts);

router.get('/products/:productId', shopController.getProduct);  // order matters for any specific url like '/products/somthing'

router.get('/cart', shopController.getCart);

router.post('/cart', shopController.postCart);

router.post('/cart-delete-item', shopController.postCartDeleteProduct);

router. post('/create-order', shopController.postOrder )

router.get('/orders', shopController.getOrders);

// router.get('/checkout', shopController.getCheckout);


module.exports = router;