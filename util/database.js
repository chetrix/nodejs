// const mysql = require('mysql2');

// const pool =mysql.createPool({
//     host: 'localhost',
//     user: 'root',
//     database: 'node-complete',
//     password: 'Rtz5kwsp@'
// });

// module.exports = pool.promise();

//SEQUELIZE >>>>>>>>>>>>>

// const Sequelize = require('sequelize');

// const sequelize = new Sequelize('node-complete', 'root', 'Rtz5kwsp@',{
//     dialect: 'mysql',
//     host: 'localhost'
// });

// module.exports = sequelize;

const mongodb = require("mongodb");
const mongoClient = mongodb.MongoClient;

let _db;

const mongoConnect = (cb) => {
  mongoClient
    .connect(
      "mongodb+srv://chetrix:rtz5kw55555@devconnector.wphwk.mongodb.net/test"
    )
    .then((client) => {
      console.log("Connected...");
      _db = client.db();
      cb();
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

const getDb=()=>{
    if(_db) {
        return _db;
    }
    throw 'No Database found!'
}

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;
